** start of undefined **

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Telephone Number Validator</title>
  <link rel="stylesheet" href="styles.css">
</head>
<body>

  <label for="user-input">Enter Phone Number:</label>
  <input type="text" id="user-input" placeholder="Enter phone number">
  <button id="check-btn">Check</button>
  <button id="clear-btn">Clear</button>
  <div id="results-div"></div>

  <script src="script.js"></script>

</body>
</html>


** end of undefined **

** start of undefined **

body {
  font-family: Arial, sans-serif;
}

#results-div {
  margin-top: 10px;
}


** end of undefined **

** start of undefined **

document.getElementById('check-btn').addEventListener('click', validatePhoneNumber);
document.getElementById('clear-btn').addEventListener('click', clearResults);

function validatePhoneNumber() {
  const userInput = document.getElementById('user-input').value;
  const resultsDiv = document.getElementById('results-div');

  if (!userInput.trim()) {
    alert('Please provide a phone number');
    return;
  }

  // Regular expression for validating US phone numbers
  const phoneRegex = /^1?[-.\s]?(\([2-9]\d{2}\)|[2-9]\d{2})[-.\s]?[2-9]\d{2}[-.\s]?\d{4}$/;

  if (phoneRegex.test(userInput)) {
    resultsDiv.innerText = `Valid US number: ${userInput}`;
  } else {
    resultsDiv.innerText = `Invalid US number: ${userInput}`;
  }
}

function clearResults() {
  document.getElementById('results-div').innerText = '';
}


** end of undefined **

