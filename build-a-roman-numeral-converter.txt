** start of undefined **

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="styles.css">
  <title>Roman Numeral Converter</title>
</head>
<body>

<div id="app">
  <input type="number" id="number" placeholder="Enter a number">
  <button id="convert-btn" onclick="convertToRoman()">Convert</button>
  <div id="output"></div>
</div>

<script src="script.js"></script>

</body>
</html>


** end of undefined **

** start of undefined **

body {
  font-family: Arial, sans-serif;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  margin: 0;
}

#app {
  text-align: center;
}

input {
  padding: 10px;
  margin-right: 10px;
}

button {
  padding: 10px;
  cursor: pointer;
}

#output {
  margin-top: 20px;
  font-weight: bold;
}


** end of undefined **

** start of undefined **

document.getElementById('convert-btn').addEventListener('click', function() {
    // Get the input value
    var inputNumber = document.getElementById('number').value;

    // Validate input
    if (inputNumber === '') {
        document.getElementById('output').innerText = 'Please enter a valid number';
    } else if (inputNumber < 1) {
        document.getElementById('output').innerText = 'Please enter a number greater than or equal to 1';
    } else if (inputNumber >= 4000) {
        document.getElementById('output').innerText = 'Please enter a number less than or equal to 3999';
    } else {
        // Convert the number to Roman numeral
        var romanNumeral = convertToRoman(inputNumber);
        document.getElementById('output').innerText = romanNumeral;
    }
});

function convertToRoman(num) {
    var romanNumeral = '';
    var romanValues = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
    var romanSymbols = ['M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV', 'I'];

    for (var i = 0; i < romanValues.length; i++) {
        while (num >= romanValues[i]) {
            romanNumeral += romanSymbols[i];
            num -= romanValues[i];
        }
    }

    return romanNumeral;
}


** end of undefined **

